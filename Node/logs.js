// source : https://www.npmjs.com/package/winston-elasticsearch

var winston = require('winston');
var Elasticsearch = require('winston-elasticsearch');

// Create the elasticsearch transporter
var esTransportOpts = {
  clientOpts: { node: 'http://localhost:9200' },
  index: 'logs'
}
// create the pipeline
var logger = winston.createLogger({
  transports: [
    new Elasticsearch(esTransportOpts)
  ]
});

// Send logs to elk
logger.log({
  level:'debug',
  message:'Send to elasticsearch !'
});
// Send logs to elk
logger.log({
  level:'info',
  message:'Send to info elasticsearch !'
});
// Send logs to elk
logger.log({
  level:'info',
  message:'Send to 2nd info to elasticsearch !'
});

logger.info('Some message', {});


// Send logs to elk
logger.log({
  level:'info',
  message:'Salut Louis!'
});
