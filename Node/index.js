'use strict';

const express = require('express');

// Constants
const PORT = 8080;
const HOST = '0.0.0.0';

// App
const app = express();
app.get('/login', (req, res) => {
  res.send('Login Page\n');
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);
