# Node Project Dashboard

![GitHub repo size](https://img.shields.io/github/repo-size/Ducrioma/NodeProjectDashboard)
![GitHub contributors](https://img.shields.io/github/contributors/Ducrioma/NodeProjectDashboard)
![Github license](https://img.shields.io/github/license/Ducrioma/NodeProjectDashboard)

Node Project Dashboard is a light `node based project` that allows `users people` to do `retrieve their data`.

This project is a school project with no application in production.
It includes DevOps practices such as containerization and external monitoring of the application.

## Features
* Login on the application
* Send metrics
* Retrieve metrics into a nice dashboard
* A dedicated work space
* ELK Stack for monitoring microservices

## Prerequisites

Before you begin, ensure you have met the following requirements:
* You have installed the latest version of `Docker` and `Docker Compose`.
* You have a `Linux/Mac` machine. Check if you have the latest version.
* You have a `Windows` machine. Have the latest version of `Node`.
* You have `docker` running for `Linux/Mac` users.

## Installing Node Dashboard

To install Node Dashboard , follow these steps:

Linux and macOS:
```
cd /YOUR_DIRECTORY/
https://github.com/Ducrioma/NodeProjectDashboard.git
cd /NodeProjectDashboard
docker-compose up
```
Windows:
```
git clone NodeProjectDashboard
cd NodeProjectDashboard
npm install && npm start
```

## Using Node Dashboard

To use the dashboard, follow these steps:
1. Open up a webbrowser, navigate to `localhost:8080` for the login page.
2. Login with your credentials
3. Input metrics and check the result in the dashboard page.
4. Open Kibana, navigate to `localhost:5601`to open the monitoring of your application.


## Contributing to Node Dashboard
To contribute to Node Dashboard, follow these steps:

1. Fork this repository.
2. Create a branch: `git checkout -b <branch_name>`.
3. Make your changes and commit them: `git commit -m '<commit_message>'`
4. Push to the original branch: `git push origin <project_name>/<location>`
5. Create the pull request.

Alternatively see the GitHub documentation on [creating a pull request](https://help.github.com/en/github/collaborating-with-issues-and-pull-requests/creating-a-pull-request).

## Contributors

Thanks to the following people who have contributed to this project:

* [@Marius](https://github.com/Ducrioma) 📖
* [@Pierre](https://github.com/Pieerre988) 🐛

## License

This project uses the following license: [MIT](https://github.com/Ducrioma/NodeProjectDashboard/blob/master/LICENSE.md).
